#include "Pizzeria.h"
#include "list"
#include "unordered_set"
#include "string"

namespace PizzeriaLibrary {

void Pizza::AddIngredient(const Ingredient &ingredient) { pIngredients.push_back(ingredient); }

int Pizza::NumIngredients() const { return pIngredients.size(); }

int Pizza::ComputePrice() const {
    int sum = 0;
    for(auto iter = pIngredients.begin(); iter != pIngredients.end(); ++iter) {
        sum += iter->Price;
    }
    return sum;
}

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price) {
    if(_ingredients.find(name) != _ingredients.end()) {
        throw runtime_error("Ingredient already inserted");
    }
    Ingredient& ingr = _ingredients[name];
    ingr.Name = name;
    ingr.Price = price;
    ingr.Description = description;

    _ingredients[name] = ingr;

}


const Ingredient& Pizzeria::FindIngredient(const string &name) const {

     try {
        _ingredients.at(name);
        for(auto itr = _ingredients.begin(); itr != _ingredients.end(); itr++) {
            if(itr->first == name) return itr->second;
        }
     }  catch (exception ex) {
        throw runtime_error("Ingredient not found");
     }

}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients) {

    if(menu.find(name) != menu.end()) {
        throw runtime_error("Pizza already inserted");
    }
        Pizza newP;
        Ingredient ingr;
        newP.Name = name;
        for(int i = 0; i < ingredients.size(); i++) {
            try{
                ingr = this->FindIngredient(ingredients[i]);
                cout << ingr.Name << " add pizza" << endl;
                newP.AddIngredient(ingr);
            }catch(exception ex) {
                continue;
            }
        }

        menu[name] = newP;

}

const Pizza &Pizzeria::FindPizza(const string &name) const {
    try {
       return menu.at(name);
        /*
       for(auto itr = menu.begin(); itr != menu.end(); itr++) {
           cout << itr->second.Name << endl;
           if(itr->first == name) return itr->second;
       }*/
    }  catch (exception ex) {
       throw runtime_error("Pizza not found");
    }

}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    Order ord;
    if(pizzas.size() == 0) throw runtime_error("Empty order");
    for(size_t i = 0; i < pizzas.size(); i++) {
        try {
            Pizza pzz = FindPizza(pizzas[i]);

            ord.AddPizza(pzz);
        } catch (exception ex) {
            cout << "something crashed here"<< endl;
            continue;
            //no pizza for u today!
        }

    }
    orders[nOrder] = ord;
    return nOrder++;
}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    try {
        Order ord = orders.at(numOrder);
        cout << "find order " << ord.NumPizzas() << endl;
        return orders.at(numOrder);
    }  catch (exception ex) {
        throw runtime_error("Order not found");
    }
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    int sum = 0;
    string phrase = "";
    //- Margherita, 5 euro\n- Marinara, 2 euro\n  TOTAL: 7 euro\n
    try {
        Order ord = orders.at(numOrder);
        for(size_t i = 0; i < ord.NumPizzas(); i++) {
            Pizza p = ord.GetPizza(i);
            phrase = phrase + "- " + p.Name + ", " + to_string(p.ComputePrice()) + " euro\n";
            sum += p.ComputePrice();
        }
        phrase = phrase + "  TOTAL: " + to_string(sum) + " euro\n";
        cout << phrase << endl;
        return phrase;
    }  catch (exception e) {
        throw runtime_error("Order not found");
    }
}

string Pizzeria::ListIngredients() const {
    string message = "";
    for(auto itr = _ingredients.begin(); itr != _ingredients.end(); itr++) {
        message = message + itr->second.Name + " - '" + itr->second.Description + "': " + to_string(itr->second.Price) + " euro\n" ;
    }
    return message;
}

string Pizzeria::Menu() const {
//"Marinara (1 ingredients): 2 euro\nMargherita (2 ingredients): 5 euro\n"
    string message = "";
    for(auto itr = menu.begin(); itr != menu.end(); itr++) {
        message =message +  itr->second.Name + " (" + to_string(itr->second.NumIngredients()) + " ingredients): "
                  + to_string(itr->second.ComputePrice()) + " euro\n";
    }
    return message;
}



}
