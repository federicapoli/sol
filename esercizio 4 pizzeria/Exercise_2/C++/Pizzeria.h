#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <list>
#include <queue>
#include <unordered_map>



using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
  };

  class Pizza {
  private:
      list<Ingredient> pIngredients;
    public:
      string Name;
      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const;
      int ComputePrice() const;
  };

  class Order {
  private:
      vector<Pizza> order;
    public:
      void InitializeOrder(int numPizzas) { throw runtime_error("Unimplemented method"); }
      void AddPizza(const Pizza& pizza) { order.push_back(pizza); }
      const Pizza& GetPizza(const int& position) const { return order[position]; }
      int NumPizzas() const { return order.size(); }
      int ComputeTotal() const {
          int sum = 0;
        for(size_t i = 0; i < NumPizzas(); i++) {
            sum += order[i].ComputePrice();
        }
        return sum;
      }
  };

  class Pizzeria {
  private:
      unordered_map<string, Pizza> menu;
      unordered_map<int, Order> orders;
      unordered_map<string, Ingredient> _ingredients;
      int nOrder = 1000;
    public:
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
