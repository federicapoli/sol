
class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description

class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.Ingredients = []           # Lista di ingredienti per ogni pizza
        self.Price = self.computePrice()

    def addIngredient(self, ingredient: Ingredient):
        self.Ingredients.append(ingredient)     # Non effettuo alcun controllo sull'input

    def numIngredients(self) -> int:
        return len(self.Ingredients)

    def computePrice(self) -> int:
        total = 0
        for ingredient in self.Ingredients:
            total += ingredient.Price
        return total


class Order:
    def __init__(self):
        self.Pizzas = []                # Lista di pizze per ogni ordine
        self.Number = 0                 # Numero dell'ordine

    def getPizza(self, position: int) -> Pizza:
        try:
            return self.Pizzas[position]
        except:
            return "Position passed is wrong"

    def initializeOrder(self, numPizzas: int):      # Il controllo sul parametro avviene nella funzione Pizzeria.createOrder()
        self.Pizzas = [None] * numPizzas

    def addPizza(self, pizza: Pizza):               # Il controllo sul parametro avviene nella funzione Pizzeria.createOrder()
        self.Pizzas.append(pizza)

    def numPizzas(self) -> int:                     # Se esiste un ordine, ha un numero di pizze maggiore di 0, quindi non controllo
        return len(self.Pizzas)

    def computeTotal(self) -> int:                  # Semplice iterazione nella lista di pizze di un ordine esistente
        total = 0
        for pizza in self.Pizzas:
            total += pizza.computePrice()
        return total

    def setNumber(self, number :int):
        if number < 0:
            raise Exception("No order with negative number")
        else:
            self.Number = number


class Pizzeria:
    def __init__ (self):
        self.Ingredients = []
        self.Pizzas = []
        self.Orders = []

    def addIngredient(self, name: str, description: str, price: int):
        if len(self.Ingredients )= =0:            # Se la lista è vuota, inserisce un ingrediente correttamente istanziato
            self.Ingredients.append(Ingredient(name, price, description))
            return
        ingredient = self.findIngredient(name)  # Altrimenti, controlla che l'ingrediente da inserire non sia in lista
        if ingredien t= ="Ingredient not found":  # Ingrediente assente: inserisco un elemento costruito con parametri in input
            ingredient = Ingredient(name, price, description)
            self.Ingredients.append(ingredient)
            return
        else:               # Ingrediente presente: lancio l'eccezione
            raise Exception("Ingredient already inserted")

    def findIngredient(self, name: str) -> Ingredient:
        found = 0   # Flag binario
        # Itero tra gli ingredienti nella pizzeria, se un elemento della lista
        # ha lo stesso nome passato in input, lo considero trovato (aggiorno il flag)
        # a 1, altrimenti restituisco una stringa di errore
        for ingredient in self.Ingredients:
            if ingredient.Name == name:
                found == 1
                return ingredient
        if found == 0:
            return "Ingredient not found"

    def addPizza(self, name: str, ingredients: []):
        # Lista delle pizze della pizzeria vuota: controllo se gli ingredienti in
        # input esistono nella lista degli ingredienti; se esistono, li aggiungo alla pizza,
        # altrimenti, lancio un'eccezione. L'inserimento della pizza avviene se tutti
        # gli ingredienti passati in input esistono
        if len(self.Pizzas) == 0:
            pizza = Pizza(name)
            for ingredient_str in ingredients:
                ingredient = self.findIngredient(ingredient_str)
                if ingredient == "Ingredient not found":
                    raise AssertionError("Ingredient not found")
                else:
                    pizza.addIngredient(ingredient)
            self.Pizzas.append(pizza)
            return
        # Se la lista delle pizze è non vuota, mantengo l'algoritmo precedente, ma mi
        # assicuro che la pizza da inserire non sia già in lista: se è così lancio un'eccezione
        else:
            pizza_find = self.findPizza(name)
            if pizza_find != "Pizza not found":           # Found pizza
                raise Exception ("Pizza already inserted")
            else:                               # Not found, can add
                pizza = Pizza(name)
                for ingredient_str in ingredients:
                    ingredient = self.findIngredient(ingredient_str)
                    if ingredient == "Ingredient not found":
                        raise AssertionError("Ingredient not found")
                    else:
                        pizza.addIngredient(ingredient)
                self.Pizzas.append(pizza)
                return

    def findPizza(self, name: str) -> Pizza:
        # Algoritmo analogo a Pizzeria.findIngredient(ingredient)
        found = 0
        for pizza in self.Pizzas:
            if pizza.Name == name:
                found = 1
                return pizza
        if found == 0:
            return "Pizza not found"

    def createOrder(self, pizzas: []) -> int:
        # Istanzio un ordine, di per sè ha una lista di pizze vuota, controllo che
        # sia passato in input una lista di pizze non vuota: se non è così, ritorno una stringa
        # di errore, altrimenti imposto il numero dell'ordine e itero sulla
        # lista passata in input.
        # Una pizza viene inserita con la funzione Order.addPizza(pizza: Pizza) se
        # viene trovata nella lista Pizzeria.Pizzas, altrimenti lo script si interrompe.
        # Se tutte le pizze passate in input vengono inserite, l'ordine è valido e quindi
        # lo inserisco nella lista Pizzeria.Orders
        order = Order()
        if len(pizzas )= =0:
            raise AssertionError("Empty order")
        else:
            order.setNumber(100 0 +len(self.Orders))
            for pizza in pizzas:                        #
                pizza_find = self.findPizza(pizza)
                if pizza_find == "Pizza not found":
                    return 0
                else:
                    order.addPizza(pizza_find)
            self.Orders.append(order)
            return order.Number

    def findOrder(self, numOrder: int) -> Order:
        # Ogni ordine ha un numero univoco, quindi cerco nella lista in modo autoesplicativo
        for order in self.Orders:
            if order.Number == numOrder:
                return order
        raise AssertionError("Order not found")

    def getReceipt(self, numOrder: int) -> str:
        # Cerco l'ordine nella lista Pizzeria.Orders: se non lo trovo, restituisco una
        # stringa di errore, altrimenti itero sulla lista di pizze dell'ordine trovato
        # e concateno una stringa (argomento del +=). Alla fine delle iterazioni
        # aggiungo un'ulteriore stringa del totale
        receipt = ""
        order = self.findOrder(numOrder)
        if order != "Order not found":
            for pizza in order.Pizzas:
                receipt += ("- " + pizza.Name + ", " + str(pizza.computePrice()) + " euro" + "\n")
            receipt += ("  TOTAL: " + str(order.computeTotal()) + " euro" + "\n")
            return receipt
        else:
            return "Order not found"

    def listIngredients(self) -> str:
        # Ragionamento analogo alla stampa di Pizzeria.getReceipt(num: int),
        # se vuoi puoi inserire la condizione in cui la Pizzeria.Ingredients
        # abbia lunghezza nulla, ma ai fini del test va bene così
        list_ings = ""
        for ingredient in self.Ingredients:
            list_ings += (ingredient.Name + " - '" + ingredient.Description + "': " + str
                (ingredient.Price) + " euro" + "\n")
        return list_ings

    def menu(self) -> str:
        # Ragionamento analogo alla stampa di Pizzeria.getReceipt(num: int),
        # si puo inserire la condizione in cui la Pizzeria.Pizzas
        # abbia lunghezza nulla, ma ai fini del test va bene così
        list_pizzas = ""
        for pizza in self.Pizzas:
            list_pizzas += (pizza.Name + " (" + str(pizza.numIngredients()) + " ingredients): " + str
                (pizza.computePrice()) + " euro" + "\n")
        return list_pizzas