#include "shape.h"
#include <math.h>
#define M_PI           3.14159265358979323846 // PI constant

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
      double perimeter = 2*M_PI *(sqrt((pow(_a, 2) + pow(_b, 2))/2));
      if(perimeter < 0) return 0;
      else return perimeter;
  }

  Triangle::Triangle(const Point& p1, // A
                     const Point& p2, // B
                     const Point& p3) // C
  {
      //Calcolato lunghezza lati triangolo
      AB = sqrt(pow(p2.X - p1.X, 2.0) + pow(p2.Y - p1.Y, 2.0));
      AC = sqrt(pow(p3.X - p1.X, 2.0) + pow(p3.Y - p1.Y, 2.0));
      BC = sqrt(pow(p3.X - p2.X, 2.0) + pow(p3.Y - p2.Y, 2.0));
      AddVertex(p1);
      AddVertex(p2);
      AddVertex(p3);

  }

  double Triangle::Perimeter() const
  {

    return AB+AC+BC;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge)
  {
    AB = edge;
    BC = edge;
    AC = edge;
    AddVertex(p1);
  }



  Quadrilateral::Quadrilateral(const Point& p1, // A
                               const Point& p2, // B
                               const Point& p3, // C
                               const Point& p4) // D
  {
      AddVertex(p1);
      AddVertex(p2);
      AddVertex(p3);
      AddVertex(p4);
      AB = sqrt(pow(p2.X - p1.X, 2.0) + pow(p2.Y - p1.Y, 2.0));
      BC = sqrt(pow(p3.X - p2.X, 2.0) + pow(p3.Y - p2.Y, 2.0));
      CD = sqrt(pow(p4.X - p3.X, 2.0) + pow(p4.Y - p3.Y, 2.0));
      DA = sqrt(pow(p1.X - p4.X, 2.0) + pow(p1.Y - p4.Y, 2.0));

  }

  double Quadrilateral::Perimeter() const
  {
    if(points.size() == 4) {
      double _AB = sqrt(pow(points[1].X - points[0].X, 2.0) + pow(points[1].Y - points[0].Y, 2.0));
      double _BC = sqrt(pow(points[2].X - points[1].X, 2.0) + pow(points[2].Y - points[1].Y, 2.0));
      double _CD = sqrt(pow(points[3].X - points[2].X, 2.0) + pow(points[3].Y - points[2].Y, 2.0));
      double _DA = sqrt(pow(points[0].X - points[3].X, 2.0) + pow(points[0].Y - points[3].Y, 2.0));
          return _AB+_BC+_CD+_DA;
    } else {
           return AB+BC+CD+DA;
       }

  }

  Rectangle::Rectangle(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
  {

      AB = CD = sqrt(pow(p2.X - p1.X, 2.0) + pow(p2.Y - p1.Y, 2.0));
      BC = DA = sqrt(pow(p3.X - p2.X, 2.0) + pow(p3.Y - p2.Y, 2.0));
      AddVertex(p1);
      AddVertex(p2);
      AddVertex(p3);
      AddVertex(p4);
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height)
  {
      AB = height;
      BC = base;
      CD = height;
      DA = base;
      AddVertex(p1);

  }

  Point Point::operator+(const Point& point) const
  {
      Point p(X, Y);
      p.X = p.X + point.X;
      p.Y = p.Y + point.Y;
      return p;
  }

  Point Point::operator-(const Point& point) const
  {
      Point p(X, Y);
      p.X = p.X - point.X;
      p.Y = p.Y - point.Y;
      return p;
  }

  Point&Point::operator-=(const Point& point)
  {
      X -= point.X;
      Y -= point.Y;

      return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;

      return *this;
  }
}
