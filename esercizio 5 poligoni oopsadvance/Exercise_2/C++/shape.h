#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
#include <string>
using namespace std;

namespace ShapeLibrary {

//definizione classe Punto( due double:ascissa e oridinata)
//costruttore predefinito per poter inizializzare i valori a (0,0)
  class Point {
    public:
      double X;
      double Y;
      Point() { }
      Point(const double& x,
            const double& y) {
          X = x;
          Y = y;
      }

      //calcolo norma euclidea (norma2)
      double ComputeNorm2() const { return sqrt(pow((this->X),2)+pow((this->Y),2));}

      //definisco opratori +,-,-=,+= -> restituiscono un Punto
      //servono per definire operazioni sulle classi
      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);

      //friend:

      //operatore che permette di visualizzare il Punto
      friend ostream& operator<<(ostream& stream, const Point& point)
      {

        stream << "Point: x = " << point.X << " y = " << point.Y << "\n";
        return stream;
      }
  };

  //Interfaccia:
  //metodi virtual:

  class IPolygon {

    public:
      virtual double Perimeter() const = 0;           //calcolo perimetro
      virtual void AddVertex(const Point& point) = 0; //aggiuntavertice

      //operatori che confrontano il perimetro di due poligoni (sx,dx)
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){
        if(fabs(lhs.Perimeter()) - fabs(rhs.Perimeter()) < 1e-8) return true;
        else return false;
      }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){
          if(fabs(lhs.Perimeter()) - fabs(rhs.Perimeter()) > 1e-8) return true;
          else return false;
      }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){
          if(fabs(lhs.Perimeter()) - fabs(rhs.Perimeter()) <= 0) return true;
          else return false;
      }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){
          if(fabs(lhs.Perimeter()) - fabs(rhs.Perimeter()) >= 0) return true;
          else return false;
      }

  };

  //classe Ellisse:eredita tutti i membri dell'iterfaccia Ipolygon
  class Ellipse : public IPolygon
  {
  private:
      vector<Point> vertex;        //vertici:vettore di oggetti classe Punto
    public:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse() { }                   //costruttore di default
      Ellipse(const Point& center,
              const double& a,   //semiasse minore
              const double& b) { //semiasse maggiore
        _a = a;
        _b = b;
        _center = center;
      }
      virtual ~Ellipse() { }                       //distruttore virtual
      void AddVertex(const Point& point) {
          vertex.push_back(point); //aggiungo in coda al vettore dei vertici un Punto
      }
      double Perimeter() const;  //metodo ereditato da Ipolygon
  };

  //classe Cerchio,eredita da classe ellisse (e quindi anche da Ipolygon)
  class Circle : public Ellipse
  {
  private:

      double _radius;    //raggio
    public:
      Circle() { }       //costruttore di default
      Circle(const Point& center,
             const double& radius) {
        _center = center;
        _radius = radius;
        _a = radius;
        _b = radius;
      }
      virtual ~Circle() { }   //distruttore
  };

//classe triangolo,eredita da IPolygon
  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;    //vettore di oggetti classe Punto->colleziono i vertici
      double AB;               //lati triangolo
      double AC;
      double BC;
    public:
      Triangle() { }                      //costruttore
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      void AddVertex(const Point& point) {  //aggiungo in coa al vettore dei vertci
        points.push_back(point);
      }
      double Perimeter() const;         //ereditato da Ipolygon
  };


 //Triangolo equilatero , eredito da Triangolo(quindi anche da Ipolygon)
  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const double& edge);
  };

  //classe quadrilatero,eredita da Ipoligon
  class Quadrilateral : public IPolygon
  {
    public:
      vector<Point> points;        //vettore di ogg.Punto in cui inserisco i vertici
      double AB;                   //lati
      double BC;
      double CD;
      double DA;
    public:
      Quadrilateral() { }               //costruttore
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }      //distruttore
      void AddVertex(const Point& p) {  //aggiungo vertice in coda
          points.push_back(p);
      }

      double Perimeter() const;        //perimetro che eredito da Ipolygon
  };


  //rettangolo,eredita da quadrilatero
  class Rectangle : public Quadrilateral
  {

    public:
      Rectangle() { }        //costruttore
      Rectangle(const Point& p1, // A       B--------------C
                const Point& p2, // B       |              |  calculate base:   AD = Sqrt((Dx - Ax)^2 + (Dy - Ay)^2)
                const Point& p3, // C       |              |  calculate height: AB = Sqrt((Bx - Ax)^2 + (By - Ay)^2)
                const Point& p4);//D
      Rectangle(const Point& p1,        //altro modo per calcolare perimetro
                const double& base,
                const double& height);
      virtual ~Rectangle() { }    //distruttore
  };
//quadrato che eredita da rettangolo
  class Square: public Rectangle
  {
  private:
      double _edge;            //lato
    public:
      Square() { }              //costruttore
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4) {
          AddVertex(p1);
          AddVertex(p2);
          AddVertex(p3);
          AddVertex(p4);
         _edge=sqrt(pow(p2.X - p1.X, 2.0) + pow(p2.Y - p1.Y, 2.0));

      }
      Square(const Point& p1,
             const double& edge) {
        _edge = edge;                 //inizializzo lato
        AB = BC = CD = DA = edge;
        AddVertex(p1);
      }
      virtual ~Square() { }          //distruttore
  };
}

#endif // SHAPE_H
