#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{

}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{  planeNormalPointer= &planeNormal;
   planeTranslationPointer= &planeTranslation;
 return;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{  lineOriginPointer=&lineOrigin;
   lineTangentPointer=&lineTangent;

  return;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{   intersectionType = NoInteresection;

    //cnd.parallelismo
    double z= (*planeNormalPointer).transpose() * *lineTangentPointer;
    if(z>= toleranceParallelism){


    double firstterm=((*planeNormalPointer)*(*planeTranslationPointer)).squaredNorm();
    double secondterm=(*planeNormalPointer).transpose() * *lineOriginPointer;
    intersectionParametricCoordinate = (firstterm-secondterm)/z;
    intersectionType= PointIntersection;
    IntersectionPoint();
    return true;

    }
    else{ //normale e tangente sono parallele-> non ci sono inters
        double check=(*planeNormalPointer).transpose() * *lineOriginPointer;
        if(check <= toleranceIntersection){
            //la linea giace sul piano
            intersectionType = Coplanar;
            return false;
        }

    }


  return false;
}
