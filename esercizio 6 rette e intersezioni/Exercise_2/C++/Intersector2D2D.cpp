#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{

}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
   matrixNomalVector.row(0)=planeNormal;
   rightHandSide(0)= planeTranslation;

  return;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{  matrixNomalVector.row(1)=planeNormal;
   rightHandSide(1)=planeTranslation;

  return ;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    //parallelismo
    intersectionType= NoInteresection;
    tangentLine = Vector3d(matrixNomalVector(0,1)*matrixNomalVector(1,2)-matrixNomalVector(0,2)*matrixNomalVector(1,1),
                           -(matrixNomalVector(0,0)*matrixNomalVector(1,2)-matrixNomalVector(0,2)*matrixNomalVector(1,0)),
                          matrixNomalVector(0,0)*matrixNomalVector(1,1)-matrixNomalVector(0,1)*matrixNomalVector(1,0));
    double check = (( matrixNomalVector.row(0).transpose())*rightHandSide(0)-
                  ( matrixNomalVector.row(1).transpose())*rightHandSide(1)).squaredNorm();
    if(tangentLine.squaredNorm() <= toleranceParallelism){
        //quando sono paralleli
        if(abs(check) <= toleranceIntersection){
            //sono coincidenti
            intersectionType = Coplanar;
            return false;
        }
        else {
            //non si intersecano
            intersectionType = NoInteresection;
            return false;
        }

    }
    else { //si intersecano
        intersectionType=LineIntersection;
        return true;

         }




  return false;
}
