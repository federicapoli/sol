#include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

BusStation::BusStation(const string &busFilePath) {
    this->_busFilePath = busFilePath;
}

void BusStation::Load() {
    /// Reset station
        _numberOfBuses = 0;
        _buses.clear();

        /// Open File
        ifstream file;
        file.open(_busFilePath.c_str());

        if (file.fail())
          throw runtime_error("Something goes wrong");

        /// Load buses
        try {
          string line;
          getline(file, line); // Skip Comment Line
          getline(file, line); // Get Line with number of buses
          istringstream convertN;
          convertN.str(line);
          convertN >> _numberOfBuses;
          getline(file, line); // Skip Comment Line
          getline(file, line); // Skip Comment Line


          /// Fill Buses
          _buses.resize(_numberOfBuses);
          for (int b = 0; b < _numberOfBuses; b++)
          {
             getline(file, line); // Start get fuelCost
             _buses[b].Id = b + 1;
             _buses[b].FuelCost = stoi(line.substr(2));
          }
          /// Close File
          file.close();
        } catch (exception) {
          _numberOfBuses = 0;
          _buses.clear();

          throw runtime_error("Something goes wrong");
        }
}

void MapData::Load() {
    /// Open File
        ifstream file;
        file.open(_mapFilePath.c_str());

        if (file.fail())
          throw runtime_error("Something goes wrong");

        /// Load
        try {
          string line;

          /// Get busStops
          getline(file, line); // Skip Comment Line
          getline(file, line);
          istringstream convertBusStops;
          convertBusStops.str(line);
          convertBusStops >> _numBusStops;

          getline(file, line); // Skip Comment Line
          _busStops.resize(_numBusStops);
          for (int b = 0; b < _numBusStops; b++)
          {
            getline(file, line);
            istringstream converter;
            converter.str(line);
            converter >> _busStops[b].Id >> _busStops[b].Name >> _busStops[b].Latitude >> _busStops[b].Longitude;
          }

          /// Get streets
          getline(file, line); // Skip Comment Line
          getline(file, line);
          istringstream convertStreets;
          convertStreets.str(line);
          convertStreets >> _numberStreets;

          getline(file, line); // Skip Comment Line
          _streets.resize(_numberStreets);
          _streetsFrom.resize(_numberStreets);
          _streetsTo.resize(_numberStreets);
          for (int s = 0; s < _numberStreets; s++)
          {
            getline(file, line);
            istringstream converter;
            converter.str(line);
            converter >> _streets[s].Id >> _streetsFrom[s] >> _streetsTo[s] >> _streets[s].TravelTime;
          }

          /// Get routes
          getline(file, line); // Skip Comment Line
          getline(file, line);
          istringstream convertRoutes;
          convertRoutes.str(line);
          convertRoutes >> _numberRoutes;

          getline(file, line); // Skip Comment Line
          _routes.resize(_numberRoutes);
          _routeStreets.resize(_numberRoutes);
          for (int r = 0; r < _numberRoutes; r++)
          {
            getline(file, line);
            istringstream converter;
            converter.str(line);
            converter >> _routes[r].Id >> _routes[r].NumberStreets;
            _routeStreets[r].resize(_routes[r].NumberStreets);

            for (int s = 0; s < _routes[r].NumberStreets; s++)
              converter >> _routeStreets[r][s];
          }

          /// Close File
          file.close();
        } catch (exception) {
          throw runtime_error("Something goes wrong");
        }
}

int MapData::NumberBusStops() const { return _numBusStops; }

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const {
    if (idRoute > _numberRoutes)
          throw runtime_error("Route " + to_string(idRoute) + " does not exists");
        const Route& route = GetRoute(idRoute);
        if (streetPosition >= route.NumberStreets)
          throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");
        int idStreet = _routeStreets[idRoute - 1][streetPosition];
        return GetStreet(idStreet);
}

const Route &MapData::GetRoute(const int &idRoute) const {
    if(_numberRoutes < idRoute) throw runtime_error("Route " + to_string(idRoute) + " does not exists");
    else {
        return _routes[idRoute-1];
    }
}

const Street &MapData::GetStreet(const int &idStreet) const {
    if(_numberStreets < idStreet) throw runtime_error("Street " + to_string(idStreet) + " does not exists");
    else {
        return _streets[idStreet-1];
    }
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const {
    if (idStreet > _numberStreets)
          throw runtime_error("Street " + to_string(idStreet) + " does not exists");
        int idFrom = _streetsFrom[idStreet - 1];
        return _busStops[idFrom - 1];
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const {
    if (idStreet > _numberStreets)
          throw runtime_error("Street " + to_string(idStreet) + " does not exists");
        int idTo = _streetsTo[idStreet - 1];
        return _busStops[idTo - 1];
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const {
    if(_numBusStops < idBusStop) throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");
    else {
        return _busStops[idBusStop-1];
    }
}

string MapViewer::ViewRoute(const int &idRoute) const {
    const Route& route = _mapData.GetRoute(idRoute);

    int s = 0;
    ostringstream routeView;
    routeView<< to_string(idRoute)<< ": ";
    for (; s < route.NumberStreets - 1; s++)
    {
      int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
      string from = _mapData.GetStreetFrom(idStreet).Name;
      routeView << from << " -> ";
    }

    int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
    string from = _mapData.GetStreetFrom(idStreet).Name;
    string to = _mapData.GetStreetTo(idStreet).Name;
    routeView << from<< " -> "<< to;

    return routeView.str();
}

string MapViewer::ViewStreet(const int &idStreet) const {
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);
    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const {
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);
    return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", " + to_string((double)busStop.Longitude / 10000.0) + ")";
}




}

