#ifndef VIAMICHELIN_H
#define VIAMICHELIN_H

#include <iostream>
#include <exception>

#include "bus.h"
#include <vector>
#include <string>

using namespace std;
using namespace BusLibrary;

namespace ViaMichelinLibrary {

  class BusStation : public IBusStation {
  private:
      string _busFilePath;
      vector<Bus> _buses;
      int _numberOfBuses = 0; // could be get from buses size
    public:
      BusStation(const string& busFilePath);

      void Load();
      int NumberBuses() const {
          return _numberOfBuses;
      };
      const Bus& GetBus(const int& idBus) const {
          if(NumberBuses() < idBus) throw runtime_error("Bus " + to_string(idBus) + " does not exists");
          else {
              return _buses[idBus-1];
          }
          return _buses[idBus-1];

      };
  };

  class MapData : public IMapData {
  private:
      string _mapFilePath;
      int _numBusStops = 0;
      vector<BusStop> _busStops;

      int _numberStreets = 0;
      vector<Street> _streets;
      vector<int> _streetsFrom;
      vector<int> _streetsTo;

      int _numberRoutes = 0;
      vector<Route> _routes;
      vector<vector<int>> _routeStreets;
    public:
      MapData(const string& mapFilePath) {
        _mapFilePath = mapFilePath;
      }
      void Load();
      int NumberRoutes() const { return _numberRoutes; }
      int NumberStreets() const { return _numberStreets; }
      int NumberBusStops() const;
      const Street& GetRouteStreet(const int& idRoute, const int& streetPosition) const;
      const Route& GetRoute(const int& idRoute) const;
      const Street& GetStreet(const int& idStreet) const;
      const BusStop& GetStreetFrom(const int& idStreet) const;
      const BusStop& GetStreetTo(const int& idStreet) const ;
      const BusStop& GetBusStop(const int& idBusStop) const;
  };

  class RoutePlanner : public IRoutePlanner {
    public:
      static int BusAverageSpeed;

  private:
        const IMapData& _mapData;
        const IBusStation& _busStation;

    public:
      RoutePlanner(const IMapData& mapData,
                   const IBusStation& busStation) : _mapData(mapData), _busStation(busStation) {}

      int ComputeRouteTravelTime(const int& idRoute) const {

          const Route& route = _mapData.GetRoute(idRoute);
              int travelTime = 0;
              for (int s = 0; s < route.NumberStreets; s++) {
                travelTime += _mapData.GetRouteStreet(idRoute, s).TravelTime;
              }
              return travelTime;
      }
      int ComputeRouteCost(const int& idBus, const int& idRoute) const {
          const Route& route = _mapData.GetRoute(idRoute);
          const Bus& bus = _busStation.GetBus(idBus);
              double travelCost = 0.0;
              for (int s = 0; s < route.NumberStreets; s++) {
                travelCost += _mapData.GetRouteStreet(idRoute, s).TravelTime;
              }
              travelCost = travelCost/3600.0;
              return travelCost * (double)bus.FuelCost * (double)BusAverageSpeed;
      }
  };

  class MapViewer : public IMapViewer {
  private:
       const IMapData& _mapData;
    public:
      MapViewer(const IMapData& mapData) : _mapData(mapData) { }
      string ViewRoute(const int& idRoute) const;
      string ViewStreet(const int& idStreet) const;
      string ViewBusStop(const int& idBusStop) const;
  };
}

#endif // VIAMICHELIN_H
