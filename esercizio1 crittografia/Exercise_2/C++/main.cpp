#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl;
    return -1;
  }
  string password = argv[1];

  string inputFileName = "./text.txt", text;
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl;

  string decryptedText;
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

bool ImportText(const string& inputFilePath,
                string& text)
{ ifstream file;
  file.open(inputFilePath);
  if(file.fail()){
      cerr<<"errore apertura file"<<endl;
      return false;
      }
  while(!file.eof()){
      getline(file,text);
      break;
  }
   file.close();

   return true;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{ unsigned int j=0;
    unsigned int t=0,p=0,n=0;
    unsigned int l=(int)text.length();
    unsigned int keyl=(int)password.length();
    char s;
    encryptedText="";
    for(unsigned int i=0;i<l;i++){
        t=(int)text[i];
        p=(int)password[j];
        if(p<65 && p>90) {
            cout<<"ERRORE:la password deve contenere solo caratteri maiuscoli"<<endl;
            exit(-1);
        }
        n=t+p;
        s=(char)n;
        encryptedText+=s;
        j++;
        if(j>keyl)
            j=0;
    }

  return true;
}

bool Decrypt(const string& text,
             const string& password,
             string& decryptedText)
{
    unsigned int j=0;
       unsigned int t=0,p=0,n=0;
       unsigned int l=(int)text.length();
       unsigned int keyl=(int)password.length();
       char c;
       decryptedText="";
       for(unsigned int i=0;i<l;i++){
           t=(int)text[i];
           p=(int)password[j];
           if(p<65 && p>90){
               cout<<"ERRORE:la password deve contenere solo caratteri maiuscoli"<<endl;
               exit(-1);
           }
           n=t-p;
           c=(char)n;
           decryptedText+=c;
           j++;
           if(j>keyl)
               j=0;}





  return  true; }
