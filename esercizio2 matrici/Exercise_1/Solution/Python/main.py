import numpy as np
from scipy.linalg import lu_factor, lu_solve


def shape(n: int, m: int = 0):
    """
    shape   Shaped matrix.
        shape(n,m) is the n-by-m matrix with elements from 1 to n * m.
    """
    A = np.empty((n, m))
    counter: int = 0
    for i in range(0, n):
        for j in range(0, m):
            A[i, j] = 1. + counter
            counter += 1

    # Alternative
    # A = 1. + np.arange(n * m).reshape(n, m)
    return A


def rand(n: int, m: int = 0):
    """
    rand   Random matrix.
        rand(n,m) is the n-by-m matrix with random elements.
    """
    A = np.random.rand(n, m)
    return A


def hilb(n: int, m: int = 0):
    """
    hilb   Hilbert matrix.
       hilb(n,m) is the n-by-m matrix with elements 1/(i+j-1).
       it is a famous example of a badly conditioned matrix.
       cond(hilb(n)) grows like exp(3.5*n).
       hilb(n) is symmetric positive definite, totally positive, and a
       Hankel matrix.
    """
    if n < 1 or m < 0:
        raise ValueError("Matrix size must be one or greater")
    elif n == 1 and (m == 0 or m == 1):
        return np.array([[1]])
    elif m == 0:
        m = n

    A = np.empty((n, m))
    for i in range(0, n):
        for j in range(0, m):
            A[i, j] = 1 / (i + j + 1)

    # Alternative
    # A = 1. / (np.arange(1, n + 1) + np.arange(0, m)[:, np.newaxis])
    return A


def solveSystem(A):
    """
    solveSystem   Solve system with matrix.
       solveSystem(A) solve the linear system Ax=b with x ones.
       returns the determinant, the condition number and the relative error
    """

    size = A.shape[0]
    condA = np.linalg.cond(A)
    detA = np.linalg.det(A)
    b = A.sum(axis=1)[:, np.newaxis]
    solution = np.ones((size, 1))

    if abs(1. / condA) > 1e-16:
        lu, piv = lu_factor(A)
        x = lu_solve((lu, piv), b)
    else:
        x = np.ones((size, 1))

    errRel = np.linalg.norm(solution - x) / np.linalg.norm(solution)
    return detA, condA, errRel


if __name__ == '__main__':
    n: int = 4

    [detAS, condAS, errRelS] = solveSystem(shape(n, n))
    print("{:} - DetA: {:.4e}, RCondA: {:.4e}, Relative Error: {:.4e}".format('shape', detAS, 1. / condAS, errRelS))
    [detAR, condAR, errRelR] = solveSystem(rand(n, n))
    print("{:} - DetA: {:.4e}, RCondA: {:.4e}, Relative Error: {:.4e}".format('rand', detAR, 1. / condAR, errRelR))
    [detAH, condAH, errRelH] = solveSystem(hilb(n, n))
    print("{:} - DetA: {:.4e}, RCondA: {:.4e}, Relative Error: {:.4e}".format('hilb', detAH, 1. / condAH, errRelH))

    exit(0)
