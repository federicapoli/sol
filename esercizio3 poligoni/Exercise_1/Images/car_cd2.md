```plantuml
@startuml

interface ICarFactory {
  +{abstract} Car Create(color)
}
note left of ICarFactory: Create()\n  creates an instance of a car

class Ford
class Toyota
class Volkswagen

class Car {
  +Car(string producer, string model, string color)
  +string Show()
}
note left of Car: Show()\n  returns the color, producer\n  and model of the car


ICarFactory <|.. Ford  : implements
ICarFactory <|.. Toyota  : implements
ICarFactory <|.. Volkswagen  : implements
Car "*" <-- "1" ICarFactory : creates 

@enduml
```