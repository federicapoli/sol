from unittest import TestCase

import car2 as car_library


class TestCar(TestCase):
    def test_show(self):
        car = car_library.Car("Fiat", "Mustang", "Red")
        self.assertEqual(car.show(), "Mustang (Fiat): color Red")


class TestICarFactory(TestCase):
    def test_create_ford(self):
        car = car_library.Ford().create("Red")
        self.assertTrue(car)
        self.assertEqual(car.show(), "Mustang (Ford): color Red")

    def test_create_toyota(self):
        car = car_library.Toyota().create("Red")
        self.assertTrue(car)
        self.assertEqual(car.show(), "Prius (Toyota): color Red")

    def test_create_volkswagen(self):
        car = car_library.Volkswagen().create("Red")
        self.assertTrue(car)
        self.assertEqual(car.show(), "Golf (Volkswagen): color Red")
