#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    _x=x;
    _y=y;
}

Point::Point(const Point& point)
{
    _x=point._x;
    _y=point._y;

}




Ellipse::Ellipse(const Point &center, const int &a, const int &b):_c(center)
{
    _a=a;
    _b=b;

}

double Ellipse::Area() const { return M_PI*_a*_b ;
                               //implementazione calcolo area
                             }

Circle::Circle(const Point &center, const int &radius):_c1(center)
{
    _r=radius;

}

double Circle::Area() const
{
    return M_PI*_r*_r;
}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3):_p1(p1),_p2(p2),_p3(p3){}





double Triangle::Area() const
{

    float l1,l2,l3,P;
    l1=sqrt((_p1._y-_p2._y)*(_p1._y-_p2._y)+(_p1._x-_p2._x)*(_p1._x-_p2._x));
    l2=sqrt((_p3._y-_p2._y)*(_p3._y-_p2._y)+(_p3._x-_p2._x)*(_p3._x-_p2._x));
    l3=sqrt((_p3._y-_p1._y)*(_p3._y-_p1._y)+(_p3._x-_p1._x)*(_p3._x-_p1._x));
    P=(l1+l2+l3)/2;
    return sqrt(P*(P-l1)*(P-l2)*(P-l3));

}

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge) :_p1(p1)
{
    lato=edge;
}

double TriangleEquilateral::Area() const
{
    return (sqrt(3)/4)*lato*lato;
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4):_p1(p1),_p2(p2),_p3(p3),_p4(p4)
{}

double Quadrilateral::Area() const
{

    return (abs((_p1._x*_p2._y)-(_p2._x*_p1._y))+abs((_p2._x*_p3._y)-(_p3._x*_p2._y))+abs((_p3._x*_p4._y)-(_p4._x*_p3._y))+abs((_p4._x*_p1._y)-(_p1._x*_p4._y)))/2;

}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4):_p1(p1),_p2(p2),_p4(p4)
{}

double Parallelogram::Area() const
{
    float ipo,base,diff,h;
    diff= abs(_p4._x-_p1._x);

        ipo=sqrt((_p1._y-_p4._y)*(_p1._y-_p4._y)+(_p1._x-_p4._x)*(_p1._x-_p4._x));

        h=sqrt((ipo*ipo)-(diff*diff));
        base= sqrt((_p1._y-_p2._y)*(_p1._y-_p2._y)+(_p1._x-_p2._x)*(_p1._x-_p2._x));


        return base*h;
}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height):_p1(p1)
{ _b=base;
  _h=height;

}

double Rectangle::Area() const
{
    return _b*_h;
}

Square::Square(const Point &p1, const int &edge):_p1(p1)
{
    _l=edge;
}

double Square::Area() const
{
    return _l*_l;
}


}
