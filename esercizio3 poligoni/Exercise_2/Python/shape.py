import math

class Point:
    def __init__(self, x: float, y: float):
        self._x = x
        self._y = y
    def getX(self):
        return self._x
    def getY(self):
        return self._y

        pass


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.__center = center
        self.__a = a
        self.__b = b


    def area(self) -> float:
        return math.pi * self.__a * self.__b




class Circle(IPolygon):
    def __init__(self, center: Point, radius: int):
        self.__c =center
        self.__r =radius
    def area(self) -> float:
        return math.pi * self.__r * self.__r

class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.__p1 = p1
        self.__p2 = p2
        self.__p3 = p3

    def area(self) -> float:
        l1 = math.sqrt((self.__p1.getY() - self.__p2.getY()) **2  + (self.__p1.getX() - self.__p2.getX())**2)
        l2 = math.sqrt((self.__p3.getY() - self.__p2.getY()) **2  + (self.__p3.getX() - self.__p2.getX())**2)
        l3 = math.sqrt((self.__p1.getY() - self.__p3.getY()) **2  + (self.__p1.getX() - self.__p3.getX())**2)


        P = (l1 + l2 + l3) / 2
        return math.sqrt(P*(P-l1)*(P-l2)*(P-l3))






class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.__p1=p1
        self.__l=edge
    def area(self) -> float:
        return (math.sqrt(3)/4)*self.__l*self.__l

class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.__p1 = p1
        self.__p2 = p2
        self.__p3 = p3
        self.__p4 = p4
    def area(self) -> float:
        return (abs((self.__p1.getX()*self.__p2.getY())-(self.__p2.getX()*self.__p1.getY()))+abs((self.__p2.getX()*self.__p3.getY())-(self.__p3.getX()*self.__p2.getY()))+abs((self.__p3.getX()*self.__p4.getY())-(self.__p4.getX()*self.__p3.getY()))+abs((self.__p4.getX()*self.__p1.getY())-(self.__p1.getX()*self.__p4.getY())))/2


class Parallelogram(IPolygon):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        self.__p1 = p1
        self.__p2 = p2
        self.__p4 = p4

    def area(self) -> float:
        self.__diff = abs(self.__p4.getX() - self.__p1.getX())

        self.__ipo = math.sqrt((self.__p1.getY() -self.__p4.getY()) **2 + (self.__diff)**2)

        self.__h = math.sqrt((self.__ipo)**2 - (self.__diff)**2)
        self.__base = math.sqrt((self.__p1.getY() -self.__p2.getY())**2 + (self.__p1.getX() -self.__p2.getX())**2)

        return self.__base * self.__h


class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        self.__p1 = p1
        self.__h= height
        self.__b = base

    def area(self) -> float:
        return  self.__b*self.__h



class Square(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.__p1 = p1
        self.__l = edge
    def area(self) -> float:
        return  self.__l*self.__l

